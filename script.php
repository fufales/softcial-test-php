<?php


// Check if can make a word
function can_make_word($word)
{
    // Collections array
    $abcBlock = array(
        'BO',
        'XK',
        'DQ',
        'CP',
        'NA',
        'GT',
        'RE',
        'TG',
        'QD',
        'FS',
        'JW',
        'HU',
        'VI',
        'AN',
        'OB',
        'ER',
        'FS',
        'LY',
        'PC',
        'ZM'
    );

    // Uppercase Word value
    $word = strtoupper($word);
    // Split the word value
    $lettersArray = str_split($word);

    // Number of each letter per word
    $eachLetterPerWord = array_count_values($lettersArray);

    // Counter array
    $timesInArray = array();

    // Do something on each letter
    foreach ($lettersArray as $key => $letter)
    {
        $eachLetterCounter = 0;

        foreach($abcBlock as $keys => $collection)
        {
            // Each collection = $collection | Return example: 'ZM'

            // Split the collection letters in 2 each | 'Z' , 'M'
            $split_collection = str_split($collection);

            // Check if letter is on array
            if (in_array($letter, $split_collection))
            {
                $eachLetterCounter++;
                // Push keys into timesInArray
                $timesInArray[$letter] = $eachLetterCounter;
            }
        }
    }

    // Get an array with the number of letter per collection
    $NumberOfLettersPerColl = $timesInArray;

    foreach ($NumberOfLettersPerColl as $key => $letter)
    {
        $qtyOnCollection = $NumberOfLettersPerColl[$key];
        $qtyOnWord = $eachLetterPerWord[$key];

        if ($qtyOnWord <= $qtyOnCollection)
        {
            $results = '>>> can_make_word("'.$word.'")'."\r\n".' true';
        } else
        {
            $results = '>>> can_make_word("'.$word.'")'."\r\n".' false';
            break;
        }
    }

    // Print results
    echo $results;
}
// Check if can make a word END



can_make_word('CONFUSE');

?>