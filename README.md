# Softcial php test


## Script Results
These are the results according to requirements.

```
>>> can_make_word("A")
True

>>> can_make_word("BARK")
True

>>> can_make_word("BOOK")
True

>>> can_make_word("TREAT")
True

>>> can_make_word("COMMON")
False

>>> can_make_word("SQUAD")
True

>>> can_make_word("CONFUSE")
True
```

---
## Erros found
According with the example document one of the word results should be like this:

```
>>> can_make_word("BOOK")
False
```

Result is not `true`, since the real result is the next one:
```
>>> can_make_word("BOOK")
True
```

---
## Usage `>Console`
1. Open your console.
2. Run the follow command into the same folder project.

Run the next command
```
php script.php
```

And change the word `YOUR_WORD_HERE` on Line 89:
```
can_make_word('YOUR_WORD_HERE');
```

> You need to instal PHP on your environment.

---

## Script Documentation
The script follows a list of rules described into the test document.

### Resolution summary
1. Get the `$word` value from the function param.
2. Split the `$word` value into a new array called `$lettersArray`.
3. Get number of letters per word as `$eachLetterPerWord`, this way you can use the number as index.
4. Start two `foreach` functions with a counter called `$timesInArray`,  this way you know how many times is a letter in the array collectons.
    1. Foreach 1: `foreach ($lettersArray as $key => $letter)`.
    2. Foreach 2: `foreach($abcBlock as $keys => $collection)`. Here each collection return the letters itself `Return example: 'ZM'`.
    3. Get current collection and split as array called `$split_collection`;
    4. Then you can check if the letter is added to the array using the conditional `if (in_array($letter, $split_collection))`, if this is true add the values with the number of times to the array `$timesInArray[$letter] = $eachLetterCounter;` on Line 59.

When you know the number of letters found on each collection you can compare the number of letter of the collections and the number of letters of each word.

> For example: You can check how many `A` found in the array.

--- 

## Compare: Example of `confuse` word comparison
```
Found in word Array
(
    [C] => 1
    [O] => 1
    [N] => 1
    [F] => 1
    [U] => 1
    [S] => 1
    [E] => 1
)
```
Compare with:
```
Found in collection Array
(
    [C] => 2
    [O] => 2
    [N] => 2
    [F] => 2
    [U] => 1
    [S] => 2
    [E] => 2
)
```

This way you can check if any word has more letters than the number of letters found in all collections.

Compare code in Line 68 -> `foreach ($NumberOfLettersPerColl as $key => $letter)`.

Compare `$qtyOnCollection` and `$qtyOnWord`.

---

### Get results
This conditional is inside the foreach function on Line 67:

```
if ($qtyOnWord <= $qtyOnCollection)
{
    $results = '>>> can_make_word("'.$word.'")'."\r\n".' true';
} else
{
    $results = '>>> can_make_word("'.$word.'")'."\r\n".' false';
    break;
}
```

---
📕 Author: Christopher Q. - On Jan 20, 2021